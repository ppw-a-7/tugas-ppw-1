"""tugas_ppw_1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView
import tugas_1.urls as tugas_1
import fitur_3.urls as fitur_3
import fitur_profile.urls as profile
import fitur_add_friend.urls as add_friend


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^status/', include(tugas_1,namespace='tugas-1')),
    url(r'^statistics/', include(fitur_3,namespace='fitur-3')),
    url(r'^profile/', include(profile,namespace='profile')),
    url(r'^friends/', include(add_friend, namespace='friends')),
    url(r'^$', RedirectView.as_view(url='/status/', permanent=True), name = 'index'),

]
