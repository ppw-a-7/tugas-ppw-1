from django.db import models
import datetime

class Friend(models.Model):
    name = models.CharField(max_length=30)
    url = models.URLField(max_length=128)
    created_date = models.DateTimeField(auto_now_add=True)
