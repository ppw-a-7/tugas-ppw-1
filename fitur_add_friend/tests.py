from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, add_friend_post
from .models import Friend
from .forms import Add_Friend_Form

class AddFriendTest(TestCase):

    def test_friends_page_url_is_exist(self):
        response = Client().get('/friends/')
        self.assertEqual(response.status_code, 200)

    def test_fitur_add_friend_using_index_func(self):
        found = resolve('/friends/')
        self.assertEqual(found.func, index)

    def test_fitur_add_friend_using_add_friend_post_func(self):
        found = resolve('/friends/add_friend/')
        self.assertEqual(found.func, add_friend_post)

    def test_model_can_add_new_friend(self):
        #Creating a new activity
        new_activity = Friend.objects.create(name='Tom Riddle',url='http://ppwisfun.herokuapp.com')

        #Retrieving all available activity
        counting_all_available_friend= Friend.objects.all().count()
        self.assertEqual(counting_all_available_friend,1)

    def test_form_validation_for_blank_items(self):
        form = Add_Friend_Form(data={'name': '', 'url': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'],
            ["This field is required."]
        )
        self.assertEqual(
            form.errors['url'],
            ["This field is required."]
        )

    def test_add_friend_post_fail(self):
        response = Client().post('/friends/add_friend/', {'name': 'Anonymous', 'url': ''})
        self.assertEqual(response.status_code, 302)

    def test_add_friend_post_success_and_render_the_result(self):
        anonymous = 'Anonymous'
        url = 'http://tomriddle.herokuapp.com'
        response = Client().post('/friends/add_friend/', {'name': anonymous, 'url': url})
        self.assertEqual(response.status_code, 302)
        response = Client().get('/friends/')
        html_response = response.content.decode('utf8')
        self.assertIn(anonymous,html_response)
        self.assertIn(url,html_response)
