from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import Add_Friend_Form
from .models import Friend

response = {}

def index(request):
    response['add_friend_form'] = Add_Friend_Form
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'friends.html'
    return render(request, html, response)

def add_friend_post(request):
    form = Add_Friend_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['url'] = request.POST['url']
        new_friend = Friend(name=response['name'], url=response['url'])
        new_friend.save()
    return HttpResponseRedirect('/friends/')
