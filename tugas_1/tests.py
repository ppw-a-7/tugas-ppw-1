from django.test import TestCase, Client
from django.urls import resolve
from .views import index, add_status, delete_status
from .models import Status
from .forms import Status_Form
from fitur_profile.models import Person

def create_user():
	return Person.objects.create(first_name="Grumpy", last_name="Cat", birth_date='2012-04-04',
		gender="Male", email="grumpy@car.com", description="Kitty Ipsym dolor sit amhettt",
		photo="https://yt3.ggpht.com/-V92UP8yaNyQ/AAAAAAAAAAI/AAAAAAAAAAA/zOYDMx8Qk3c/s900-c-k-no-mo-rj-c0xffffff/photo.jpg")

class StatusUnitTest(TestCase):
	def test_status_url_is_exist(self):
		user = create_user()
		response = Client().get('/status/')
		self.assertEqual(response.status_code,200)

	def test_status_using_index_func(self):
		found = resolve('/status/')
		self.assertEqual(found.func, index)

	def test_model_can_create_status(self):
		#Creating a new activity
		status = Status.objects.create(description="test gan")

		#Retrieving all available activity
		counting_all_available_activity = Status.objects.all().count()
		self.assertEqual(counting_all_available_activity,1)

	def test_form_status_input_has_placeholder_and_css_classes(self):
		form = Status_Form()
		self.assertIn('class="status-form-textarea', form.as_p())
		self.assertIn('id="id_description', form.as_p())

	def test_form_validation_for_blank_items(self):
		form = Status_Form(data={'description': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['description'],
			["This field is required."]
		)

	def test_status_post_success_and_render_the_result(self):
		user = create_user()
		test = 'Anonymous'
		response_post = Client().post('/status/add_status', {'description': test})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/status/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)

	def test_status_post_error_and_render_the_result(self):
		user = create_user()
		test = 'Anonymous'
		response_post = Client().post('/status/add_status', {'description': ''})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/status/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)

	def test_status_delete_success(self):
		status = Status(description="test gan")
		status.save()

		response = Client().post('/status/delete_status', {'id-value': status.id})
		self.assertEqual(Status.objects.all().count(), 0)
