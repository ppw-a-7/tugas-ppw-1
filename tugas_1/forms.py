from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }

    description_attrs = {
        'type': 'text',
        'cols': 100,
        'rows': 10,
        'class': 'status-form-textarea',
        'placeholder':'Apa Kabar Anda Hari Ini?',
        'max_length' : 140,
    }

    description = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))
