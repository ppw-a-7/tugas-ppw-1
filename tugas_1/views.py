from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import Status
from fitur_profile.models import Person

# Create your views here.
response = {}
def index(request):
    profile = Person.objects.get(pk=1)
    response['profile'] = profile
    status = Status.objects.all()[::-1]
    response['status'] = status
    html = 'status.html'
    response['status_form'] = Status_Form
    return render(request, html, response)

def add_status(request):
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['description'] = request.POST['description']
        status = Status(description=response['description'])
        status.save()
        return HttpResponseRedirect('/status/')
    else:
        return HttpResponseRedirect('/status/')

def delete_status(request):
    status = Status.objects.get(id=int(request.POST.get("id-value", "")))
    status.delete()
    return HttpResponseRedirect('/status/')
