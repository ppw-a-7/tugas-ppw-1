from django.db import models

# Create your models here.

class Person(models.Model):
	first_name = models.CharField(max_length=30)
	last_name = models.CharField(max_length=30)
	birth_date = models.DateField()
	gender = models.CharField(max_length=10)
	email = models.EmailField(max_length=70)
	description = models.TextField(max_length=20000)
	photo = models.URLField()

class Expertise(models.Model):
	expertise = models.CharField(max_length=20)
	person = models.ForeignKey(Person, on_delete=models.CASCADE, related_name='expertises')
