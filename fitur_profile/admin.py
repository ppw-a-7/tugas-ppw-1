from django.contrib import admin
from .models import Person, Expertise

# Register your models here.
admin.site.register(Person)
admin.site.register(Expertise)