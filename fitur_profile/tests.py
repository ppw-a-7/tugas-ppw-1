from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .models import Person, Expertise

def create_user():
	return Person.objects.create(first_name="Grumpy", last_name="Cat", birth_date='2012-04-04',
		gender="Male", email="grumpy@car.com", description="Kitty Ipsym dolor sit amhettt",
		photo="https://yt3.ggpht.com/-V92UP8yaNyQ/AAAAAAAAAAI/AAAAAAAAAAA/zOYDMx8Qk3c/s900-c-k-no-mo-rj-c0xffffff/photo.jpg")

def create_expertise(user):
	return Expertise.objects.create(expertise="ngoding", person=user)

class FiturProfileUnitTest(TestCase):
	def test_fitur_profile_url_is_exist(self):
		person = create_user()
		expertise = create_expertise(person)
		response = Client().get('/profile/')
		self.assertEqual(response.status_code,200)

	def test_fitur_profile_using_index_func(self):
		found = resolve('/profile/')
		self.assertEqual(found.func, index)

	def test_model_can_create_person(self):
		#Creating a new activity
		person = create_user()
		expertise = create_expertise(person)

		#Retrieving all available activity
		counting_all_available_activity = Person.objects.all().count()
		counting_all_expertise = Expertise.objects.all().count()
		self.assertEqual(counting_all_available_activity,1)
		self.assertEqual(counting_all_expertise,1)

	def test_fitur_profile_contain_person_attributes(self):
		person = create_user()
		expertise = create_expertise(person)
		response= Client().get('/profile/')
		html_response = response.content.decode('utf8')
		self.assertIn(person.first_name, html_response)
		self.assertIn(person.last_name, html_response)
		self.assertIn(person.gender, html_response)
		self.assertIn(person.email, html_response)
		self.assertIn(person.description, html_response)
		self.assertIn(person.photo, html_response)
		self.assertIn(expertise.expertise, html_response)


