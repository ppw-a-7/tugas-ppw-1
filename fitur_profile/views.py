from django.shortcuts import render
from fitur_profile.models import Expertise, Person
from .models import Person

# Create your views here.
def index(request):
	# expertises = Expertise.objects.all()
	# person = Person.objects.first()
	# response = {"person": person, "expertises": expertises}
	profile = Person.objects.first()
	expertises = profile.expertises.all()
	response = {'profile': profile, 'expertises': expertises}
	return render(request, 'profile.html', response)
