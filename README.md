# Software Quality Checklist

* Continuous Delivery [![pipeline status](https://gitlab.com/ppw-a-7/tugas-ppw-1/badges/master/pipeline.svg)](https://gitlab.com/ppw-a-7/tugas-ppw-1/commits/master)
* Python Code [![coverage report](https://gitlab.com/ppw-a-7/tugas-ppw-1/badges/master/coverage.svg)](https://gitlab.com/ppw-a-7/tugas-ppw-1/commits/master)


# Details

* Nama-nama anggota kelompok :
	1. Muhammad Ezra Rizkiatama Putra
	2. Rafli Hidayat
	3. Rehan Hawari
	4. William Rumanta

* Link herokuapp : http://lignin.herokuapp.com/