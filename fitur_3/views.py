from django.shortcuts import render
from datetime import datetime, date
from fitur_profile.models import Person
from fitur_add_friend.models import Friend
from tugas_1.models import Status

# Enter your name here
# Create your views here.
def index(request):
    profile = Person.objects.get(pk=1)
    response = {'profile': profile,'friends':len(Friend.objects.all()),'feeds':len(Status.objects.all()),}
    response['latest'] = Status.objects.last()
    return render(request, 'contact.html', response)
