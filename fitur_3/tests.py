from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from fitur_profile.models import Person, Expertise

def create_user():
	return Person.objects.create(first_name="Grumpy", last_name="Cat", birth_date='2012-04-04',
		gender="Male", email="grumpy@car.com", description="Kitty Ipsym dolor sit amhettt",
		photo="https://yt3.ggpht.com/-V92UP8yaNyQ/AAAAAAAAAAI/AAAAAAAAAAA/zOYDMx8Qk3c/s900-c-k-no-mo-rj-c0xffffff/photo.jpg")

def create_expertise(user):
	return Expertise.objects.create(expertise="ngoding", person=user)

class AddFriendTest(TestCase):
# Create your tests here.
    """def statistic_page_url_is_exist(self):
        response = Client().get('/fitur-3/')
        self.assertEqual(response.status_code, 200)"""

    def test_fitur_statistic_using_index_func(self):
        found = resolve('/statistics/')
        self.assertEqual(found.func, index)

    def test_friend_counter(self):
        person = create_user()
        expertise = create_expertise(person)
        anonymous = 'Anonymous'
        url = 'http://tomriddle.herokuapp.com'
        response = Client().post('/friends/add_friend/', {'name': anonymous, 'url': url})
        self.assertEqual(response.status_code, 302)
        response = Client().get('/statistics/')
        html_response = response.content.decode('utf8')
        self.assertIn('Friends: 1 People',html_response)
        self.assertIn('Grumpy Cat',html_response)
